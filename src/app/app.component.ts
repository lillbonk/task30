import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task30';

  onLoginAttempt(loginResult:any):void{
    console.log(loginResult);
    if (loginResult.isLoggedIn){
      //redirect
      
    }
    alert(loginResult.message);
    
  };
  
  onRegisterAttempt(basicAuth:any):void{
    console.log(basicAuth);
    
    alert( basicAuth.message);
  };




}
