import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Xtb } from '@angular/compiler';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent{
  
  
  @Input() username:string;
  @Input() password:any;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter();

  private basicAuth:any = {
    username: 'Kebab-king',
    password: 'batman',
    confirmpassword: 'batman',
    message: 'Passwords do not match'
  }




  onRegisterClick() {
    if(this.basicAuth.password == this.basicAuth.confirmpassword){
     
      
      this.basicAuth.message = 'Account created'
      this.registerAttempt.emit(this.basicAuth);

    } else {
      this.basicAuth.message = 'Passwords do not match'
      this.registerAttempt.emit(this.basicAuth);
    }

    

      


    
  }
}
