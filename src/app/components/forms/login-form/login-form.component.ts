import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Input() formTitle: string;
  @Input() username:string;
  @Input() password:any;


  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();


 private loginResult:any = {
  isLoggedIn: false,
  message: ''
 };

 private counter:number = 0;


  onLoginClick(){

    this.counter++;

    if (this.counter % 2 === 0){
      this.loginResult.isLoggedIn = true;
      this.loginResult.message = 'You are now logged in'
    } else {

      {
        this.loginResult.isLoggedIn = false;
        this.loginResult.message = 'Invalid credentials'
      }
  
    }

    this.loginAttempt.emit(this.loginResult);
  }




}

